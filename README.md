# LinkWeChat

#### 平台介绍

LinkWeChat，是一款基于企业微信的开源SCRM系统，为企业构建私域流量系统的综合解决方案，显著提升企业社交运营效率！
![输入图片说明](https://images.gitee.com/uploads/images/2020/0825/144910_68578056_409467.png "屏幕截图.png")


#### 软件架构


- 前端采用Vue、Element UI。
- 后端采用Spring Boot、Spring Security、Redis & Jwt。
- 权限认证使用Jwt，支持多终端认证系统。
- 支持加载动态权限菜单，多方式轻松权限控制。
- 感谢[ruoyi-vue](https://gitee.com/y_project/RuoYi-Vue)提供后台框架

#### 内置功能


![输入图片说明](https://images.gitee.com/uploads/images/2020/0825/145413_3a0cab42_409467.png "屏幕截图.png")

#### 在线体验


1. 演示演示：http://www.topitclub.cn/
2. 交流群

![输入图片说明](https://images.gitee.com/uploads/images/2020/0924/140420_fb631f6a_409467.png "屏幕截图.png")

#### 演示图




